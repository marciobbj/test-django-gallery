from photos.models import SitePhoto


class PhotoApprovedOrNotMixin(object):

    def photos_approved(self):
        qs = SitePhoto.objects.filter(
            approved=True
        )
        return qs

    def photos_declined(self):
        qs = SitePhoto.objects.filter(
            approved=False
        )
        return qs
