from django.contrib import admin
from .models import SitePhoto, Like
# Register your models here.


admin.site.register(SitePhoto)
admin.site.register(Like)
