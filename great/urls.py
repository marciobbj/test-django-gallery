from django.conf import settings
from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path

urlpatterns = [
    path(r'admin/', admin.site.urls),
    path(r'', include('photos.urls'), name='index'),
    path(r'login/', auth_views.LoginView.as_view(), name='login-view'),
    path(r'logout/', auth_views.LogoutView.as_view(), name='logout-view'),
    path(r'photologue/', include('photologue.urls'), name='photologue'),
]

if settings.DEBUG == True:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
