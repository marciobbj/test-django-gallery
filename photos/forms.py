from django import forms

from photos.models import Like


class LikeForm(forms.ModelForm):

    class Meta:
        model = Like
        fields = '__all__'
