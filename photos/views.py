from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import redirect
from django.urls import reverse_lazy, reverse
from django.views.generic import CreateView, DeleteView, UpdateView
from photologue.views import PhotoListView, PhotoDetailView

from photos.mixins import PhotoApprovedOrNotMixin
from photos.models import SitePhoto, Like


class IndexView(PhotoApprovedOrNotMixin, PhotoListView):
    """
    This view brings my index and here I passed some other
    context like the counter of photos waiting in the queue
    """
    template_name = 'photos/gallery_index.html'
    paginate_by = 10
    context_object_name = 'photos'

    def get_queryset(self):
        if self.request.GET.get('order_by_like'):
            return sorted(
                self.photos_approved(),
                key=lambda photo: int(photo.likes),
                reverse=True
            )
        return self.photos_approved().order_by(
            'date_added'
        )

    def get_context_data(self, *args, **kwargs):
        context = super(IndexView, self).get_context_data()
        photos_to_approve = self.photos_declined().count()
        context['photos_to_approve'] = photos_to_approve
        return context


class PhotoView(PhotoApprovedOrNotMixin, PhotoDetailView):
    """
    Detail view, basicaly passing the Likes counter for
    this specific object
    """
    model = SitePhoto
    context_object_name = 'object'

    def get_context_data(self, *args, **kwargs):
        context = super(PhotoView, self).get_context_data()
        photos_to_approve = self.photos_declined().count()
        context['photos_to_approve'] = photos_to_approve
        context['likes'] = Like.objects.filter(picture_id=self.object.id)
        return context

    def get_queryset(self):
        return self.photos_approved()


class PhotosToApprove(LoginRequiredMixin, PhotoApprovedOrNotMixin, PhotoListView):
    """
    This view returns all the Photos not approved
    """
    template_name = 'photos/gallery_photos_to_approve.html'
    paginate_by = 10
    context_object_name = 'photos'

    def get_context_data(self, *args, **kwargs):
        context = super(PhotosToApprove, self).get_context_data()
        photos_to_approve = self.photos_declined().count()
        context['photos_to_approve'] = photos_to_approve
        return context

    def get_queryset(self):
        return self.photos_declined()


class PhotoLikeView(LoginRequiredMixin, CreateView):
    """
    Basically in this view, I made a custom validation
    to check whether the user already liked that specific
    picture, first I get all the likes he has in the DB
    if appears a like related to this Photo instance, it gets
    rejected and the counter does not continue. Just one like
    per user in each photo.
    """
    success_url = reverse_lazy('photos:index')
    model = Like
    fields = ['user', 'picture', ]
    queryset = Like.objects.all()

    def form_invalid(self, form):
        return redirect(reverse('photos:index'))

    def form_valid(self, form):
        picture_id = int(self.kwargs['photo_id'])
        pictures_that_user_liked = Like.objects.filter(user=form.instance.user)
        for like in pictures_that_user_liked:
            if like.picture.id == picture_id:
                return self.form_invalid(form)
        return super(PhotoLikeView, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(PhotoLikeView, self).get_form_kwargs()
        kwargs['data'] = {
            'user': self.request.user.id,
            'picture': self.kwargs['photo_id']
        }
        return kwargs


class UserUploadPhotoView(PhotoApprovedOrNotMixin, LoginRequiredMixin, CreateView):
    model = SitePhoto
    fields = ['title', 'image']
    success_url = reverse_lazy('photos:index')

    def get_context_data(self, *args, **kwargs):
        context = super(UserUploadPhotoView, self).get_context_data()
        photos_to_approve = self.photos_declined().count()
        context['photos_to_approve'] = photos_to_approve
        return context

    def form_valid(self, form):
        form.instance.user = self.request.user
        form.instance.slug = form.instance.title.replace(' ', '')
        return super(UserUploadPhotoView, self).form_valid(form)


class RejectPhotoView(PhotoApprovedOrNotMixin, DeleteView):
    model = SitePhoto
    pk_url_kwarg = 'photo_id'
    success_url = reverse_lazy('photos:to_approve')

    def get_queryset(self):
        return self.photos_declined()


class ApprovePhotoView(SuccessMessageMixin, PhotoApprovedOrNotMixin, UpdateView):
    model = SitePhoto
    fields = 'approved',
    pk_url_kwarg = 'photo_id'
    success_url = reverse_lazy('photos:to_approve')
    success_message = 'The photo is now in your feed :)'

    def get_queryset(self):
        return self.photos_declined()
