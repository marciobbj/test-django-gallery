from argparse import Action

import factory
from django.contrib.auth.models import User
from .models import SitePhoto
from faker import Faker

faker = Faker()

class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    is_staff = False
    is_active = True
    username = factory.Sequence(lambda n: f'tester{n}')
    email = factory.LazyAttribute(lambda o: f'{o}@test.com')
    password = factory.PostGenerationMethodCall('set_password', 'testpassword')


class SitePhotoFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = SitePhoto

    user = factory.SubFactory(UserFactory)
    approved = faker.boolean()

