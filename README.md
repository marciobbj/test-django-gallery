# Job test - Django Gallery

# You can see a live version [here](http://204.48.23.157)

## Here I will explain everything you need to know about this application (or try xD)

1. Setup a virtual-env (I highly suggest Pyenv w/ virtuslenv plugin, I used Python 3.6 by the way)
2. Clone this repo
3. Cd project_root > pip install -r requirements.txt
4. I recommend to use the SQLite db I already made, but if you want to play around with django permissions and stuff go ahead!!
4. **If you decided to create your own db**: `python manage.py migrate`

### If migrations error raises, go to `photo/migrations` -> delete the folder -> `mkdir migrations` -> `touch migrations/__init__.py`  

I've setted a SQlite3/MySQL database configuration in the settings.py, I suggest you to use it or
if you want to use other configuration, I know that there's a lot persons that likes PostGress :)
https://stackoverflow.com/questions/5394331/how-to-setup-postgresql-database-in-django#5421511

I think that's the only thing that can stop you from running this locally, I tried to keep very simple.

1. `python manage.py runserver`


Everything should be fine now. I will take you for a Tour in this app. 
I've already uploaded a SQlite db populated with our users groups. 
Theres two kinds of users: **Owners and Standards**. 
Owners can see options in the application that Standards cannot.

The idea of using django was because I like the way it manages it's users permissions,
with that said you can play around with this in the admin-panel, just go to `admin/` and see
how nice Django is, to get all the information login with the `testsuperadmin` account. Other users
cannot change much stuff inside admin panel, we generally try to put all user can do inside our templates.

#### I've already done some of these for testing purpose:

* Login: testsuperadmin - Pass: test123 - Group: superadmin **(for the live version use: testsuperuser -> test123)**
* Login: sue - Pass: theowner1 - Group: Owners
* Login: joey - Pass: theowner2 - Group: Owners
* Login: sammy - Pass: theregular - Group: Regular

There's already some sample data for you to play with. 
But basically, regular users can just upload a picture, 
this picture is sent to a queue that only OWNERS can see. 
So when Sue or Joey Login they will have access to the notification bar, 
on the top screen. In that place they can approve or not the picture.

*still missing some unit tests*

## That's it for now

