from django.urls import path

from .views import \
    IndexView, UserUploadPhotoView,\
    PhotosToApprove, PhotoView, \
    PhotoLikeView, RejectPhotoView, \
    ApprovePhotoView

app_name = 'photos'
urlpatterns = [
    path(r'', IndexView.as_view(), name='index'),
    path(r'<slug>', PhotoView.as_view(), name='photo_detail'),
    path(r'photo/<photo_id>/like/', PhotoLikeView.as_view(), name='photo_like'),
    path(r'approve/', PhotosToApprove.as_view(), name='to_approve'),
    path(r'approve/<photo_id>/', ApprovePhotoView.as_view(), name="approve"),
    path(r'reject/<photo_id>', RejectPhotoView.as_view(), name="reject"),
    path(r'upload/', UserUploadPhotoView.as_view(), name='upload'),
]
