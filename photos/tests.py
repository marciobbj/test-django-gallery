from django.test import TestCase
from .factories import UserFactory, SitePhotoFactory
# Create your tests here.

class UserActionsPhoto(TestCase):

    user = UserFactory(name='John', is_staff=False)


    def test_user_upload_photo(self):
        photo = SitePhotoFactory()
        data = {
            "picture": photo,
            "user": self.user
        }
        response = self.client.post(
            'upload/',
            data=data
        )
        self.assertEqual(response.status_code, 200)
