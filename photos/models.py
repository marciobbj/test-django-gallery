from django.contrib.auth import get_user_model
from django.db import models
# Create your models here.
from django.urls import reverse
from photologue.models import Photo

User = get_user_model()


class SitePhoto(Photo):
    approved = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    @property
    def likes(self):
        likes_for_this_pic = Like.objects.filter(picture_id=self.id)
        return likes_for_this_pic.count()

    def get_absolute_url(self):
        return reverse('photos:photo_detail', args=[self.slug])


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    picture = models.ForeignKey(SitePhoto, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
